jQuery.sap.declare("appr.util.util");
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {
		addResult: function(items) {
			this.items = [];
			for (var i in items) {
				var line = items[i];
				line.DetailSet = items[i].DetailSet.results;
				line.DetailAllSet = items[i].DetailAllSet.results;
				line.DetailItemSet = items[i].DetailItemSet.results;
				line.HistorySet = items[i].HistorySet.results;
				line.AttachSet = items[i].AttachSet.results;
				line.NotesSet = items[i].NotesSet.results;
				if (items[i].Wfid === "10000090") {
					// var content = items[i].DetailItemSet[0].Content;
					// var json = this.xmlToJson(content);
					// line.DetailItemSet = json;
					
					// for (var k in items[i].DetailItemSet) {
						var detailItems = items[i].DetailItemSet;
						var detailItemsNew = [];
						for (var j in detailItems) {
							var content = detailItems[j].Content;
							var json = jQuery.parseJSON(content);
							detailItemsNew.push({
								Name: detailItems[j].Name,
								Text: detailItems[j].Text,
								Content: json
							});
						}
						line.DetailItemSet = detailItemsNew;

					// }
				}
				this.items.push(line);
			}
			this.itemsFlag = true;
			// var content = this.items[43].DetailItemSet[0].Content;
			// var json = this.xmlToJson(content);

			if (this.detail != undefined && this.detail.func != undefined) {
				for (var i in this.items) {
					if (this.items[i].Wfid === this.detail.objectId && this.items[i].Apprtype === this.detail.apprtype) {
						this.detail.func(this.items[i], this.detail.t);
						return;
					}
				}
				this.detail.func(null, this.detail.t);
				return;
			}
		},

		readResult: function(objectId, apprtype, func, t) {
			if (this.itemsFlag) {
				for (var i in this.items) {
					if (this.items[i].Wfid === objectId && this.items[i].Apprtype === apprtype) {
						func(this.items[i], t);
						return this.items[i];
					}
				}
				func(null, t);
				return null;
			} else {
				this.detail = {};
				this.detail.func = func;
				this.detail.t = t;
				this.detail.objectId = objectId;
				this.detail.apprtype = apprtype;
			}
		},

		readTableCustom: function() {
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");

			var sPath = "/HeaderSet";

			oModel.read(sPath, {
				urlParameters: {
					"$expand": "DetailSet,DetailItemSet,DetailAllSet,HistorySet"
				},
				success: function(oData, response) {

					var oJSONModel = new sap.ui.model.json.JSONModel({
						HeaderCollection: oData.results
					});
					t.getView().setModel(oJSONModel, "HeaderModel");
					util.addResult(oData.results);
				},
				error: function(oError) {
					// console.log("Error" + oError.responseText)
				}
			});
		},

		//------------------------------- Busy Indicator ----------------------------------------------//
		showBusy: function() {
			if (this.busyCount == undefined) {
				this.busyCount = 1;
			} else {
				this.busyCount += 1;
			}
			sap.ui.core.BusyIndicator.show();
		},

		hideBusy: function() {
			this.busyCount -= 1;
			if (this.busyCount <= 0) {
				this.busyCount = 0;
				sap.ui.core.BusyIndicator.hide();
			}
		},

		xmlToJson: function(xml) {

			var roots = $(xml);
			var root = roots[roots.length - 1];
			var json = {};
			this.parse(root, json);
			return json.node;
		},
		parse: function(node, j) {
			var t = this;
			var nodeName = node.nodeName.replace(/^.+:/, '').toLowerCase();
			var cur = null;
			var text = $(node).contents().filter(function(x) {
				return this.nodeType === 3;
			});
			if (text[0] && text[0].nodeValue.trim()) {
				cur = text[0].nodeValue;
			} else {
				cur = {};
				$.each(node.attributes, function() {
					if (this.name.indexOf('xmlns:') !== 0) {
						cur[this.name.replace(/^.+:/, '')] = this.value;
					}
				});
				$.each(node.children, function() {
					t.parse(this, cur);
				});
			}

			j[nodeName] = cur;
		}

	};

});