/*global location */
sap.ui.define([
	"appr/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"appr/model/formatter",
	"appr/util/util",
	"sap/ui/Device"
], function(BaseController, JSONModel, formatter, util, Device) {
	"use strict";

	return BaseController.extend("appr.controller.Detail", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function() {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0,
				AttachLength: 0,
				lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading")
			});

			//-------
			var items = [];
			items.push({
				ItemNo: "10",
				Matnr: "33434003",
				Matxt: "Kaldıraç kolu",
				Quan: "2",
				Meins: "ADT",
				Netwr: "142.800,000",
				Curr: "TRY"
			});
			items.push({
				ItemNo: "20",
				Matnr: "11434550",
				Matxt: "Tekerlek",
				Quan: "4",
				Meins: "ADT",
				Netwr: "13.200,000",
				Curr: "TRY"
			});
			var oViewModel2 = new JSONModel({
				ItemCollection: items
			});
			this.setModel(oViewModel2, "ItemModel");
			var oViewModel3 = new JSONModel({
				Items: false
			});
			this.setModel(oViewModel3, "ItemModel2");

			//----

			this.isPhone = Device.system.phone;

			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

			this.setModel(oViewModel, "detailView");

			this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
		},

		getEventBus: function() {
			return sap.ui.getCore().getEventBus();
		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Event handler when the share by E-Mail button has been clicked
		 * @public
		 */
		onShareEmailPress: function() {
			var oViewModel = this.getModel("detailView");

			sap.m.URLHelper.triggerEmail(
				null,
				oViewModel.getProperty("/shareSendEmailSubject"),
				oViewModel.getProperty("/shareSendEmailMessage")
			);
		},

		/**
		 * Event handler when the share in JAM button has been clicked
		 * @public
		 */
		onShareInJamPress: function() {
			var oViewModel = this.getModel("detailView"),
				oShareDialog = sap.ui.getCore().createComponent({
					name: "sap.collaboration.components.fiori.sharing.dialog",
					settings: {
						object: {
							id: location.href,
							share: oViewModel.getProperty("/shareOnJamTitle")
						}
					}
				});

			oShareDialog.open();
		},

		/**
		 * Updates the item count within the line item table's header
		 * @param {object} oEvent an event containing the total number of items in the list
		 * @private
		 */
		onListUpdateFinished: function(oEvent) {
			var sTitle,
				iTotalItems = oEvent.getParameter("total"),
				oViewModel = this.getModel("detailView");

			// only update the counter if the length is final
			if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
				if (iTotalItems) {
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
				} else {
					//Display 'Line Items' instead of 'Line items (0)'
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
				}
				oViewModel.setProperty("/lineItemListTitle", sTitle);
			}
		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			this.sObjectId = oEvent.getParameter("arguments").objectId;
			this.Apprtype = oEvent.getParameter("arguments").apprtype;
			this.getDetailFromUtil();
			if (this.sObjectId === "10000070") {
				this.getModel("ItemModel2").setProperty("/Items", true);
			} else {
				this.getModel("ItemModel2").setProperty("/Items", false);
			}
			// this.getModel().metadataLoaded().then( function() {
			// 	var sObjectPath = this.getModel().createKey("HeaderSet", {
			// 		Wfid :  sObjectId
			// 	});
			// 	this._bindView("/" + sObjectPath);
			// }.bind(this));
		},

		getDetailFromUtil: function() {
			util.readResult(this.sObjectId, this.Apprtype, this.setDetail, this);
			this.getEventBus().publish("Master", "ApprovelList", {});
		},

		setDetail: function(items, t) {
			if (items != null) {
				for (var i in items.HistorySet) {
					if (items.HistorySet[i].Prsta === "1") {
						items.HistorySet[i].Icon = "sap-icon://pending";
						items.HistorySet[i].Color = "Orange";
					} else if (items.HistorySet[i].Prsta === "2") {
						items.HistorySet[i].Icon = "sap-icon://accept";
						items.HistorySet[i].Color = "Green";
					} else if (items.HistorySet[i].Prsta === "3") {
						items.HistorySet[i].Icon = "sap-icon://decline";
						items.HistorySet[i].Color = "Red";
					} else {
						items.HistorySet[i].Icon = "";
					}
				}
				var oJSONModel = new sap.ui.model.json.JSONModel({
					DetailCollection: items.DetailSet,
					DetailAllCollection: items.DetailAllSet,
					HistorySet: items.HistorySet,
					AttachSet: items.AttachSet,
					NotesSet: items.NotesSet
				});
				oJSONModel.setData(items);
			} else {
				oJSONModel = new sap.ui.model.json.JSONModel({
					DetailCollection: [],
					DetailAllCollection: [],
					HistorySet: [],
					AttachSet: [],
					NotesSet: []
				});
			}
			var attachLength = items.AttachSet.length;
			t.getView().setModel(oJSONModel, "DetailModel");
			t.getView().getModel("detailView").setProperty("/busy", false);
			t.getView().getModel("detailView").setProperty("/AttachLength", attachLength);
			t.detailFormId = t.byId(sap.ui.core.Fragment.createId("idDetailFragment", "idDetailForm"));
			t.setDetailFields(t.detailFormId, items.DetailSet);
			// t.detailAllFormId = t.byId(sap.ui.core.Fragment.createId("idDetailAllFragment", "idDetailAllForm"));
			// t.setDetailFields(t.detailAllFormId,items.DetailAllSet);
			// t.byId("idHistoryFragment").setModel(oJSONModel, "DetailModel");
			t.getView().bindElement({
				path: "/HistoryCollection",
				model: "DetailModel"
			});
			t.addDetailItems(items);
		},

		setDetailFields: function(form, items) {
			var t = this;
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView());
			this.dynamicFormContainerIndex = 0;
			this.dynamicSpanValue = "L8 M7 S7";
			this.dynamicSpanValueForNum = "L2 M3 S10";
			this.dynamicSpanValueForNumLabel = "L1 M1 S2";
			form.destroyFormContainers();
			items.forEach(function(item) {
				var formContainer = form.getFormContainers()[t.dynamicFormContainerIndex];
				if (formContainer === undefined) {
					var customFormContainer = new sap.ui.layout.form.FormContainer({});
					form.addFormContainer(customFormContainer);
					formContainer = form.getFormContainers()[0];
				}
				var formElement = new sap.ui.layout.form.FormElement({
					label: new sap.m.Label({
						text: item.Evfds
							// design:"Bold"
					}),
					fields: [ 
						item.Funit === "" ? t.getCustomControl(item) : t.getCustomControlNumber(item)
					]

				});

				formContainer.addFormElement(formElement);
			});
			// ///---------------------------------------------------------
			// var formContainer = form.getFormContainers()[t.dynamicFormContainerIndex];
			//     if (formContainer === undefined) {
			//         var customFormContainer = new sap.ui.layout.form.FormContainer({});
			//         form.addFormContainer(customFormContainer);
			//         formContainer = form.getFormContainers()[0];
			//     }
			//     var formElement = new sap.ui.layout.form.FormElement({
			//         label: new sap.m.Label({
			//             text: "Total"
			//             // design:"Bold"
			//         }),
			//         fields: [
			//             t.getCustomControlNumber({type:""})
			//         ],
			//     });

			//     formContainer.addFormElement(formElement);
			// ///---------------------------------------------------------

		},

		addDetailItems: function(item) {
			var oGrid = this.byId(sap.ui.core.Fragment.createId("idDetailFragment", "idGrid"));
			// destroy Dynamic Panels...
			var aggrList = oGrid.mAggregations.content;
			for (var i=0; i<aggrList.length;i++) {
				if (aggrList[i].getId().substr(0, 11) === "__panelItem") {
					aggrList[i].destroy();
					i--;
				}
			}
			
			if (item.DetailItemSet!=undefined&&item.DetailItemSet.length) {
				
				// var table = new sap.m.Table();
				for (var i in item.DetailItemSet) {//enes
					var panel = new sap.m.Panel("__panelItem"+i);
					panel.setExpandable(true);
					panel.setHeaderText(item.DetailItemSet[i].Text);
					oGrid.addContent(panel);
					var oTable = new sap.m.Table("__tableItem"+i);
					panel.addContent(oTable);
					// var oColumn = new sap.m.Column();
					var columnList = item.DetailItemSet[i].Content;
					if(!colmnFlag){
						for(var j in columnList){
						var colmnFlag = true;
						var oColumn = new sap.m.Column({
								header: new sap.m.Text({
									text: columnList[j].flfld
								})
							});
						oTable.addColumn(oColumn);
					}
					}
					
					var oText = new sap.m.Text();
					
					var tbl = this.byId("application-Test-url-component---detail--idDetailFragment--idWorkListFragment--idProductsTable");
					var clm = tbl.getColumns()[0];
					 //oColumn.addAggregation("header",oText);
				}
			}
		},

		getCustomControl: function(item) {
			var t = this;
			// var customType = new sap.ui.core.CustomData({
			// 	key: "dynamicType",
			// 	value: item.Type,
			// 	writeToDom: true
			// });

			var oText = new sap.m.Text({
				 id: t.getView().getId() + "--" + item.Flfld + "Text",
				text: item.Flfva,
				enabled: item.Enable === "" ? false : true,
				layoutData: new sap.ui.layout.GridData({
					span: t.dynamicSpanValue
				})
			});

			// oText.addCustomData(customType);
			return oText;
		},

		getCustomControlNumber: function(item) {
			var t = this;
			// var customType = new sap.ui.core.CustomData({
			// 	key: "dynamicType",
			// 	value: item.Type,
			// 	writeToDom: true
			// });

			var oObjectNumber = new sap.m.ObjectNumber({
				id: t.getView().getId() + "--" + item.Flfld + "Number",
				number: item.Flfva, //"153,000.000",
				unit: "TRY",
				// enabled: item.Enable === "" ? false : true,
				layoutData: new sap.ui.layout.GridData({
					span: t.dynamicSpanValue
				})
			});

			// oObjectNumber.addCustomData(customType);
			return oObjectNumber;
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function(sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("detailView");

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);

			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function() {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function() {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},

		_onBindingChange: function() {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}

			var sPath = oElementBinding.getPath(),
				oResourceBundle = this.getResourceBundle(),
				oObject = oView.getModel().getObject(sPath),
				sObjectId = oObject.Wfid,
				sObjectName = oObject.Evcde,
				oViewModel = this.getModel("detailView");

			this.getOwnerComponent().oListSelector.selectAListItem(sPath);

			oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("shareSaveTileAppTitle", [sObjectName]));
			oViewModel.setProperty("/shareOnJamTitle", sObjectName);
			oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage",
				oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
		},

		_onMetadataLoaded: function() {
			// Store original busy indicator delay for the detail view
			// var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
			// 	oViewModel = this.getModel("detailView"),
			// 	oLineItemTable = this.byId("list"),//lineItemsList
			// 	iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

			// // Make sure busy indicator is displayed immediately when
			// // detail view is displayed for the first time
			// oViewModel.setProperty("/delay", 0);
			// oViewModel.setProperty("/lineItemTableDelay", 0);

			// oLineItemTable.attachEventOnce("updateFinished", function() {
			// 	// Restore original busy indicator delay for line item table
			// 	oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
			// });

			// // Binding the view will set it to not busy - so the view is always busy if it is not bound
			// oViewModel.setProperty("/busy", true);
			// // Restore original busy indicator delay for the detail view
			// oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
		},

		//*----------------------------------------------------------------------------------------------------------*
		//*----------------------------------------------------------------------------------------------------------*
		//		*** APPROVE...
		//*----------------------------------------------------------------------------------------------------------*
		handleApprove: function() {
			var oJSONModel = this.getView().getModel("DetailModel");
			if (oJSONModel.oData.Evcdc === "X") {
				var descFlag = true;
			} else {
				descFlag = false;
			}
			this.getView().getModel("DetailModel").setProperty("/DescriptionFlag", descFlag);
			this.getView().getModel("DetailModel").setProperty("/Description", "");
			this.byId("idApproveDialog").setVisible(true);
			this.byId("idApproveDialog").open();
		},

		handleApproveOkPress: function() {
			this.decision("A");
			this.byId("idApproveDialog").close();
			this.byId("idApproveDialog").setVisible(false);
		},

		handleApproveCancelPress: function() {
			this.byId("idApproveDialog").close();
			this.byId("idApproveDialog").setVisible(false);
		},

		//*----------------------------------------------------------------------------------------------------------*
		//  	*** REJECT...
		//*----------------------------------------------------------------------------------------------------------*
		handleReject: function() {
			var oJSONModel = this.getView().getModel("DetailModel");
			if (oJSONModel.oData.Evcdc === "X") {
				var descFlag = true;
			} else {
				descFlag = false;
			}

			this.getView().getModel("DetailModel").setProperty("/DescriptionFlag", descFlag);
			this.getView().getModel("DetailModel").setProperty("/Description", "");
			this.byId("idRejectDialog").setVisible(true);
			this.byId("idRejectDialog").open();
		},

		handleRejectOkPress: function() {
			this.decision("R");
			this.byId("idRejectDialog").close();
			this.byId("idRejectDialog").setVisible(false);
		},

		handleRejectCancelPress: function() {
			this.byId("idRejectDialog").close();
			this.byId("idRejectDialog").setVisible(false);
		},

		//*----------------------------------------------------------------------------------------------------------*
		//  	*** DECISION...
		//*----------------------------------------------------------------------------------------------------------*		
		decision: function(decisionType) {
			var t = this;
			var desc = this.getView().getModel("DetailModel").getProperty("/Description");
			var prstp = this.getView().getModel("DetailModel").getProperty("/Prstp");
			var rfcdest = this.getView().getModel("DetailModel").getProperty("/Rfcdest");
			var extId = this.getView().getModel("DetailModel").getProperty("/ExtId");

			var oPostData = {
				Wfid: this.sObjectId,
				Prstp: prstp,
				DecisionType: decisionType,
				Description: desc,
				Rfcdest: rfcdest,
				ExtId: extId,
				Return: "",
				Message: ""
			};

			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");

			var sPath = "/DecisionSet";

			util.showBusy();
			oModel.create(sPath, oPostData, {
				success: function(oData, response) {
					util.hideBusy();
					if (oData.Return == "SUCCESS") {
						sap.m.MessageToast.show(oData.Message);
						if (t.isPhone) {
							history.go(-1);
						} else {
							t.refreshMaster();
						}
					} else {
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});
					}
				},
				error: function(err) {
					util.hideBusy();
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		},

		refreshMaster: function() {
			this.getEventBus().publish("Master", "RefreshList", {});
		},

		handleForward: function() {
			// if (!this._oDialogForward) {
			// 	this._oDialogForward = sap.ui.xmlfragment("appr.view.fragments.Forward", this);
			// }

			// this._oDialogForward.setModel(this.getView().getModel());
			// // toggle compact style
			// jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogForward);
			// this._oDialogForward.open();
			// this.getForwardUsers();

			this.byId("idForwardDialog").setVisible(true);
			this.byId("idForwardDialog").open();
		},

		handleForwardOkPress: function() {
			this.decision("R");
			this.byId("idForwardDialog").close();
			this.byId("idForwardDialog").setVisible(false);
		},

		handleForwardCancelPress: function() {
			this.byId("idForwardDialog").close();
			this.byId("idForwardDialog").setVisible(false);
		},

		getForwardUsers: function(value) {
			var t = this;
			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");

			if (value === "UserGroup") {
				var sPath = "/UserGroupSet";

				util.showBusy();
				oModel.read(sPath, {
					success: function(oData, response) {
						util.hideBusy();
						var oJSONModel = new sap.ui.model.json.JSONModel({
							UserGroupCollection: oData.results
						});
						t._oUserGroupForward.setModel(oJSONModel, "UserGroupModel");
					},
					error: function(oError) {
						util.hideBusy();
						// console.log("Error" + oError.responseText)
					}
				});

			} else if (value === "User") {

				util.showBusy();
				var sPath2 = "/UserSet";
				oModel.read(sPath2, {
					success: function(oData, response) {
						util.hideBusy();
						var oJSONModel = new sap.ui.model.json.JSONModel({
							UserCollection: oData.results
						});
						t._oUserForward.setModel(oJSONModel, "UserModel");
					},
					error: function(oError) {
						util.hideBusy();
						// console.log("Error" + oError.responseText)
					}
				});
			}
		},

		//*----------------------------------------------------------------------------------------------------------*
		//  	*** User Group...
		//*----------------------------------------------------------------------------------------------------------*
		onUserGroupButtonPress: function() {

			if (!this._oUserGroupForward) {
				this._oUserGroupForward = sap.ui.xmlfragment("appr.view.fragments.UserGroup", this);
			}
			this.getForwardUsers("UserGroup");

			this._oUserGroupForward.setModel(this.getView().getModel());
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oUserGroupForward);
			this._oUserGroupForward.open();
		},

		onLiveChangeForwardUG: function(oEvent) {
			var value = oEvent.getParameter("value");
			var oF1 = new sap.ui.model.Filter("Uname", sap.ui.model.FilterOperator.Contains, value);
			var oF2 = new sap.ui.model.Filter("Description", sap.ui.model.FilterOperator.Contains, value);

			var oFilters2 = new sap.ui.model.Filter({
				filters: [
					oF1,
					oF2
				],
				and: false
			});
			this._oUserGroupForward.getBinding("items").filter(oFilters2, sap.ui.model.FilterType.Application);
		},

		onUserGroupSelect: function(oEvent) {
			var oCell = oEvent.getParameter("selectedItem").getCells()[0];
			var usrgrp = oCell.getTitle();
			this.forward(usrgrp);
		},

		//*----------------------------------------------------------------------------------------------------------*
		//  	*** User...
		//*----------------------------------------------------------------------------------------------------------*
		onUserButtonPress: function() {

			if (!this._oUserForward) {
				this._oUserForward = sap.ui.xmlfragment("appr.view.fragments.User", this);
			}
			this.getForwardUsers("User");

			this._oUserForward.setModel(this.getView().getModel());
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oUserForward);
			this._oUserForward.open();
		},

		onLiveChangeForwardU: function(oEvent) {
			var value = oEvent.getParameter("value");
			var oF1 = new sap.ui.model.Filter("Uname", sap.ui.model.FilterOperator.Contains, value);
			var oF2 = new sap.ui.model.Filter("Name", sap.ui.model.FilterOperator.Contains, value);

			var oFilters2 = new sap.ui.model.Filter({
				filters: [
					oF1,
					oF2
				],
				and: false
			});
			this._oUserForward.getBinding("items").filter(oFilters2, sap.ui.model.FilterType.Application);
		},

		onUserSelect: function(oEvent) {
			var oCell = oEvent.getParameter("selectedItem").getCells()[0];
			var usr = oCell.getTitle();
			this.forward(usr);
		},

		//*----------------------------------------------------------------------------------------------------------*
		//  	*** Forward oData...
		//*----------------------------------------------------------------------------------------------------------*
		forward: function(uname) {
			var t = this;

			var rfcdest = this.getView().getModel("DetailModel").getProperty("/Rfcdest");
			var extId = this.getView().getModel("DetailModel").getProperty("/ExtId");

			var oPostData = {
				Wfid: this.sObjectId,
				Uname: uname,
				Rfcdest: rfcdest,
				ExtId: extId,
				Return: "",
				Message: ""
			};

			var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");

			var sPath = "/ForwardSet";

			util.showBusy();
			oModel.create(sPath, oPostData, {
				success: function(oData, response) {
					util.hideBusy();
					if (oData.Return == "SUCCESS") {
						sap.m.MessageToast.show(oData.Message);
						t.byId("idForwardDialog").close();
						t.byId("idForwardDialog").setVisible(false);
						if (t.isPhone) {
							history.go(-1);
						} else {
							t.refreshMaster();
						}
					} else {
						sap.m.MessageToast.show(oData.Message, {
							duration: 5000
						});
					}
				},
				error: function(err) {
					util.hideBusy();
					// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
				}
			});
		}

	});

});