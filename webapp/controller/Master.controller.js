/*global history */
sap.ui.define([
		"appr/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator",
		"sap/m/GroupHeaderListItem",
		"sap/ui/Device",
		"appr/model/formatter",
		"appr/util/util"
	], function (BaseController, JSONModel, History, Filter, FilterOperator, GroupHeaderListItem, Device, formatter,util) {
		"use strict";

		return BaseController.extend("appr.controller.Master", {

			formatter: formatter,

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			/**
			 * Called when the master list controller is instantiated. It sets up the event handling for the master/detail communication and other lifecycle tasks.
			 * @public
			 */
			onInit : function () {
				// Control state model
				var oList = this.byId("list"),
					oViewModel = this._createViewModel(),
					// Put down master list's original value for busy indicator delay,
					// so it can be restored later on. Busy handling on the master list is
					// taken care of by the master list itself.
					iOriginalBusyDelay = oList.getBusyIndicatorDelay();
				
				this.isPhone = Device.system.phone;
				
				var date = new Date();
				date.setMonth(date.getMonth() - 2);
				this.startDate = date;
				this.startDate2 = formatter.dateToOdataFormat2(this.startDate);
				
				var oJSONModel = new sap.ui.model.json.JSONModel({
					SelectedEvent:"",
					DateValue:this.startDate
				});
				this.getView().setModel(oJSONModel, "FilterModel");
				
				this._oList = oList;
				// keeps the filter and search state
				this._oListFilterState = {
					aFilter : [],
					aSearch : []
				};

				this.setModel(oViewModel, "masterView");
				// Make sure, busy indication is showing immediately so there is no
				// break after the busy indication for loading the view's meta data is
				// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
				oList.attachEventOnce("updateFinished", function(){
					// Restore original busy indicator delay for the list
					oViewModel.setProperty("/delay", iOriginalBusyDelay);
				});

				this.getView().addEventDelegate({
					onBeforeFirstShow: function () {
						this.getOwnerComponent().oListSelector.setBoundMasterList(oList);
					}.bind(this)
				});
    			var oEventBus = this.getEventBus();
        		oEventBus.subscribe("Master", "ApprovelList", this.readTableCustom, this);
        		oEventBus.subscribe("Master", "RefreshList", this.refreshList, this);

				this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
				this.getRouter().attachBypassed(this.onBypassed, this);
			},
			
			getEventBus: function() {
        		return sap.ui.getCore().getEventBus();
    		},
    		
    		onExit: function(){
		        var oEventBus = this.getEventBus();
		        oEventBus.unsubscribe("Master", "ApprovelList", this.readTableCustom, this);
    		},

			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * After list data is available, this handler method updates the
			 * master list counter and hides the pull to refresh control, if
			 * necessary.
			 * @param {sap.ui.base.Event} oEvent the update finished event
			 * @public
			 */
			onUpdateFinished : function (oEvent) {
				// // update the master list object counter after new data is loaded
				// this._updateListItemCount(oEvent.getParameter("total"));
				// // hide pull to refresh if necessary
				// this.byId("pullToRefresh").hide();
				// this.setItemsColor();
			},
			
			setItemsColor: function(){
				var oItems = this.byId("list").getItems();
				for (var i = 0; i < oItems.length; i++) {
					var attr = oItems[i].getAttributes()[1];
					// attr.addStyleClass("yellow");
		            // if (oTitle.addStyleClass != undefined) {
		            //     // if (oItems[i].getAttributes()[2].getText() === "I0045" ||oItems[i].getAttributes()[2].getText() === "I0046") {
		            //     //     oTitle.addStyleClass("fieldRed");
		            //     // } else {
		            //     //     oTitle.addStyleClass("fieldGreen");
		            //     // }
		            // }
		        }
			},
			/**
			 * Event handler for the master search field. Applies current
			 * filter value and triggers a new search. If the search field's
			 * 'refresh' button has been pressed, no new search is triggered
			 * and the list binding is refresh instead.
			 * @param {sap.ui.base.Event} oEvent the search event
			 * @public
			 */
			onSearch : function (oEvent) {
				if (oEvent.getParameters().refreshButtonPressed) {
					// Search field's 'refresh' button has been pressed.
					// This is visible if you select any master list item.
					// In this case no new search is triggered, we only
					// refresh the list binding.
					// this.onRefresh();
					return;
				}

				var sQuery = oEvent.getParameter("query");

				if (sQuery) {
					this._oListFilterState.aSearch = [new Filter("Evcde", FilterOperator.Contains, sQuery)];
				} else {
					this._oListFilterState.aSearch = [];
				}
				this._applyFilterSearch();

			},

			/**
			 * Event handler for refresh event. Keeps filter, sort
			 * and group settings and refreshes the list binding.
			 * @public
			 */
			onRefresh : function () {
				// this._oList.getBinding("items").refresh();
				this.onRefreshFlag = true;
				this.refreshList();
			},
			
			onLiveChange: function(oEvent){
				if(oEvent.getParameter("refreshButtonPressed")){
					this.onRefresh();
				}else{
	        		var value = oEvent.getSource().getValue();
					this.setFilter();
				}
			},
			
			refreshList: function(){
				this.approvelListFlag = false;
				this.readTableCustom();
			},
			
			onSelectChangeList: function(oEvent){
				this.setFilter();
			},
			
			setFilter: function(){
				var oF1 = this.getApprTypeFilter();
        		var oF2 = this.getSearchFilter();
        		var oF3 = this.getFilter();
		        
		        var oFilters2 = new sap.ui.model.Filter({
		            filters: [
		                oF1,
		                oF2,
		                oF3
		            ],
		            and: true
		        });
		        this.byId("list").getBinding("items").filter(oFilters2, sap.ui.model.FilterType.Application);
			},
			
			getSearchFilter: function(){
				var value = this.byId("searchField").getValue();
				var oF1 = new sap.ui.model.Filter("Wfid", sap.ui.model.FilterOperator.Contains, value);
				var oF2 = new sap.ui.model.Filter("Flkey", sap.ui.model.FilterOperator.Contains, value);
				var oF3 = new sap.ui.model.Filter("Evcde", sap.ui.model.FilterOperator.Contains, value);
		        return new sap.ui.model.Filter({
		            filters: [
		                oF1,
		                oF2,
		                oF3
		            ],
		            and: false
		        });
			},
			
			getApprTypeFilter: function(){
				var filters = [];
				var key = this.byId("idSelectList").getSelectedKey();
				switch (key) {
					case "1": //me...
						var oF1 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "01");
						break;
					case "2": //group...
						var oF1 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "02");
						break;
					case "3":
						var oF1 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "01");
						var oF2 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "02");
						break;
					case "4":
						var oF1 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "04");
						break;
					case "5":
						var oF1 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "01");
						var oF2 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "02");
						var oF3 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "04");
						break;
					case "6":
						var oF1 = new sap.ui.model.Filter("Apprtype", sap.ui.model.FilterOperator.EQ, "06");
						break;
					default:
				}
				
				if(oF1){
					filters.push(oF1);
				}
				
				if(oF2){
					filters.push(oF2);
				}
				
				if(oF3){
					filters.push(oF3);
				}
				
				return new sap.ui.model.Filter({
		            filters: filters,
		            and: false
		        });
			},

			/**
			 * Event handler for the list selection event
			 * @param {sap.ui.base.Event} oEvent the list selectionChange event
			 * @public
			 */
			onSelectionChange : function (oEvent) {
				// get the list item, either from the listItem parameter or from the event's source itself (will depend on the device-dependent mode).
				this._showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
			},

			/**
			 * Event handler for the bypassed event, which is fired when no routing pattern matched.
			 * If there was an object selected in the master list, that selection is removed.
			 * @public
			 */
			onBypassed : function () {
				this._oList.removeSelections(true);
			},

			/**
			 * Used to create GroupHeaders with non-capitalized caption.
			 * These headers are inserted into the master list to
			 * group the master list's items.
			 * @param {Object} oGroup group whose text is to be displayed
			 * @public
			 * @returns {sap.m.GroupHeaderListItem} group header with non-capitalized caption.
			 */
			createGroupHeader : function (oGroup) {
				return new GroupHeaderListItem({
					title : oGroup.text,
					upperCase : false
				});
			},

			/**
			 * Event handler for navigating back.
			 * It there is a history entry or an previous app-to-app navigation we go one step back in the browser history
			 * If not, it will navigate to the shell home
			 * @public
			 */
			onNavBack : function() {
				var sPreviousHash = History.getInstance().getPreviousHash(),
					oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");

				if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
					history.go(-1);
				} else {
					oCrossAppNavigator.toExternal({
						target: {shellHash: "#Shell-home"}
					});
				}
			},

			/* =========================================================== */
			/* begin: internal methods                                     */
			/* =========================================================== */


			_createViewModel : function() {
				return new JSONModel({
					isFilterBarVisible: false,
					filterBarLabel: "",
					delay: 0,
					title: this.getResourceBundle().getText("masterTitleCount", [0]),
					noDataText: "Kayıt yok",//this.getResourceBundle().getText("masterListNoDataText"),
					sortBy: "Evcde",
					groupBy: "None"
				});
			},

			/**
			 * If the master route was hit (empty hash) we have to set
			 * the hash to to the first item in the list as soon as the
			 * listLoading is done and the first item in the list is known
			 * @private
			 */
			_onMasterMatched :  function() {
			// 	this.getOwnerComponent().oListSelector.oWhenListLoadingIsDone.then(
			// 		function (mParams) {
			// 			if (mParams.list.getMode() === "None") {
			// 				return;
			// 			}
			// 			var sObjectId = mParams.firstListitem.getBindingContext().getProperty("Wfid");
			// 			this.getRouter().navTo("object", {objectId : sObjectId}, true);
			// 		}.bind(this),
			// 		function (mParams) {
			// 			if (mParams.error) {
			// 				return;
			// 			}
			// 			this.getRouter().getTargets().display("detailNoObjectsAvailable");
			// 		}.bind(this)
			// 	);
				if(this.isPhone){
					this.approvelListFlag=false;
				}
				this.readTableCustom();
			
			},
			
			readTableCustom: function(){
				if(!this.approvelListFlag){
					this.approvelListFlag = true;
					var t = this;
					var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");
					
					var sPath = "/HeaderSet";
					
					var oFilter = this.getExpandFilter();
					util.showBusy();
					oModel.read(sPath, {
						filters:[oFilter],
						urlParameters: {
					                "$expand": "DetailSet,DetailAllSet,DetailItemSet,HistorySet,AttachSet,NotesSet"
					            },
						success: function(oData, response) {
							util.hideBusy();
							t.ApprovalList = oData.results;
							for(var i in t.ApprovalList){
								if(t.ApprovalList[i].Prstt==="F"){
									t.ApprovalList[i].PrsttText = t.ApprovalList[i].Flcnm+" tarafından yönlendirildi";
								}else if(t.ApprovalList[i].Prstt==="R"){
									t.ApprovalList[i].PrsttText = t.ApprovalList[i].Flcnm+" tarafından reddedildi";
								}
							}
							var oJSONModel = new sap.ui.model.json.JSONModel({
								HeaderCollection:oData.results
							});
							t.getView().setModel(oJSONModel, "HeaderModel");
							t.setFilter();
							util.addResult(oData.results);
							t.setListCount(t.ApprovalList);
							if(!t.isPhone){
								t.selectedFirstItem();
							}
							if(t.onRefreshFlag){
								sap.m.MessageToast.show("Liste güncellendi");
							}
						},
						error: function(oError) {
							util.hideBusy();
							// console.log("Error" + oError.responseText)
						}
					});	
				}
			},
			
			getExpandFilter: function(){
				
				var oModel = this.getView().getModel("FilterModel");
				var dateValue = oModel.getProperty("/DateValue");
				var date = formatter.dateToOdataFormat2(dateValue);
				var dt2 = formatter.dateToOdataFormat(date);
				var sDate = new sap.ui.model.Filter("StartDate", sap.ui.model.FilterOperator.EQ, dt2);
				
				return new sap.ui.model.Filter({
					filters: [sDate],
					and: true
				});
			},
			
			selectedFirstItem: function(){
				var oList = this.byId("list");
				var oItem = oList.getItems()[0];
				if(!oItem){
					this.getRouter().getTargets().display("detailNoObjectsAvailable");
				}else{
					oList.setSelectedItem(oItem.getId());
					this._showDetail(oItem);
				}
			},
			
			setListCount: function(items){
				var count_01 = 0;
				var count_02 = 0;
				var count_04 = 0;
				var count_06 = 0;
				for(var i in items){
					
					var oJSONModel = this.getView().getModel("FilterModel");
					if(!oJSONModel){
					}else{
						var event = oJSONModel.getProperty("/SelectedEvent");
						if(event!==""){
							if(items[i].Event!==event){
								continue;
							}
						}
					}
					
					if(items[i].Apprtype==="01"){
						count_01++;
					}else if(items[i].Apprtype==="02"){
						count_02++;
					}else if(items[i].Apprtype==="04"){
						count_04++;
					}else if(items[i].Apprtype==="06"){
						count_06++;
					}
				}
				var id1 = this.byId("id1");
				if(count_01==0){
					id1.setText("Onayımda Bekleyenler");
				}else{
					id1.setText("Onayımda Bekleyenler ("+count_01+")");
				}
				
				var id2 = this.byId("id2");
				if(count_02==0){
					id2.setText("Grup Onayında Bekleyenler");
				}else{
					id2.setText("Grup Onayında Bekleyenler ("+count_02+")");
				}
				
				var id3 = this.byId("id3");
				var count_03 = count_01 + count_02;
				if(count_03==0){
					id3.setText("Benim ve Grup Onayında Bekleyenler");
				}else{
					id3.setText("Benim ve Grup Onayında Bekleyenler ("+count_03+")");
				}
				
				var id4 = this.byId("id4");
				if(count_04==0){
					id4.setText("Vekaleten Bekleyenler");
				}else{
					id4.setText("Vekaleten Bekleyenler ("+count_04+")");
				}
				
				var id5 = this.byId("id5");
				var count_05 = count_03 + count_04;
				if(count_05==0){
					id5.setText("Tüm Bekleyen Onaylar");
					this.byId("page").setTitle("Onay Listesi");
				}else{
					id5.setText("Tüm Bekleyen Onaylar ("+count_05+")");
					this.byId("page").setTitle("Onay Listesi ("+count_05+")");
				}
				
				
				var id6 = this.byId("id6");
				if(count_06==0){
					id6.setText("Taleplerim");
				}else{
					id6.setText("Taleplerim ("+count_06+")");
				}
				
			},

			/**
			 * Shows the selected item on the detail page
			 * On phones a additional history entry is created
			 * @param {sap.m.ObjectListItem} oItem selected Item
			 * @private
			 */
			_showDetail : function (oItem) {
				var bReplace = !Device.system.phone;
				this.getRouter().navTo("object", {
					objectId : oItem.getBindingContext("HeaderModel").getProperty("Wfid"),
					apprtype:oItem.getBindingContext("HeaderModel").getProperty("Apprtype")
				}, bReplace);
			},

			/**
			 * Sets the item count on the master list header
			 * @param {integer} iTotalItems the total number of items in the list
			 * @private
			 */
			_updateListItemCount : function (iTotalItems) {
				var sTitle;
				// only update the counter if the length is final
				if (this._oList.getBinding("items").isLengthFinal()) {
					sTitle = this.getResourceBundle().getText("masterTitleCount", [iTotalItems]);
					this.getModel("masterView").setProperty("/title", sTitle);
				}
			},

			/**
			 * Internal helper method to apply both filter and search state together on the list binding
			 * @private
			 */
			_applyFilterSearch : function () {
				var aFilters = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter),
					oViewModel = this.getModel("masterView");
				this._oList.getBinding("items").filter(aFilters, "Application");
				// changes the noDataText of the list in case there are no filter results
				if (aFilters.length !== 0) {
					oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
				} else if (this._oListFilterState.aSearch.length > 0) {
					// only reset the no data text to default when no new search was triggered
					oViewModel.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
				}
			},

			/**
			 * Internal helper method to apply both group and sort state together on the list binding
			 * @param {sap.ui.model.Sorter[]} aSorters an array of sorters
			 * @private
			 */
			_applyGroupSort : function (aSorters) {
				this._oList.getBinding("items").sort(aSorters);
			},

			/**
			 * Internal helper method that sets the filter bar visibility property and the label's caption to be shown
			 * @param {string} sFilterBarText the selected filter value
			 * @private
			 */
			_updateFilterBar : function (sFilterBarText) {
				var oViewModel = this.getModel("masterView");
				oViewModel.setProperty("/isFilterBarVisible", (this._oListFilterState.aFilter.length > 0));
				oViewModel.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [sFilterBarText]));
			},
			
//*----------------------------------------------------------------------------------------------------------*
//  	*** Filter...
//*----------------------------------------------------------------------------------------------------------*
			onFilter: function(){
				this.byId("idFilterDialog").setVisible(true);
				this.byId("idFilterDialog").open();
				if(!this.FilterDataReadFlag){
					this.FilterDataReadFlag=true;
					
					// var oJSONModel = new sap.ui.model.json.JSONModel({
					// 	SelectedEvent:"",
					// 	DateValue:this.startDate
					// });
					// this.getView().setModel(oJSONModel, "FilterModel");
					this.getFilterData();
				}
				this.filterModel = {
					SelectedEvent :this.getView().getModel("FilterModel").getProperty("/SelectedEvent"),
					DateValue :this.getView().getModel("FilterModel").getProperty("/DateValue")
				};
			},
			
			handleFilterOkPress: function(){
				var oJSONModel = this.getView().getModel("FilterModel");
				var dateValue = oJSONModel.getProperty("/DateValue");
				var date = formatter.dateToOdataFormat2(dateValue);
				if(oJSONModel.getProperty("/SelectedEvent")!=="" || date !== this.startDate2){
					this.byId("idFilterButton").setType("Emphasized");
				}else{
					this.byId("idFilterButton").setType("Default");
				}
				this.setFilter();
				this.setListCount(this.ApprovalList);
				this.byId("idFilterDialog").close();
				this.byId("idFilterDialog").setVisible(false);
				
				if(this.date == undefined && date === this.startDate2){
					this.date = this.startDate2;
				}else if(date!== this.date){
					this.date= date;
					this.approvelListFlag=false;
					this.readTableCustom();
				}
			},
			
			handleFilterCancelPress: function(){
				var oJSONModel = new sap.ui.model.json.JSONModel();
				oJSONModel.setData(this.filterModel);
				this.getView().setModel(oJSONModel, "FilterModel");
				this.byId("idFilterDialog").close();
				this.byId("idFilterDialog").setVisible(false);
			},
			
			handleFilterClearPress: function(){
				var oJSONModel = new sap.ui.model.json.JSONModel({
					SelectedEvent:"",
					DateValue:this.startDate
				});
				this.getView().setModel(oJSONModel, "FilterModel");
			},
			
			getFilterData: function(){
				var t = this;
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");
				
				var sPath = "/EventSet";
				
				util.showBusy();
				oModel.read(sPath, {
					success: function(oData, response) {
						util.hideBusy();
						var items = oData.results;
						items.unshift({
							Key:"",
							Text:""
						});
						var oJSONModel = new sap.ui.model.json.JSONModel({
							EventCollection:items,
						});
						t.getView().setModel(oJSONModel, "EventModel");
					},
					error: function(oError) {
						util.hideBusy();
					}
				});	
			},
			
			getFilter: function(){
				var oJSONModel = this.getView().getModel("FilterModel");
				if(!oJSONModel){
					var event = "";
				}else{
					var event = oJSONModel.getProperty("/SelectedEvent");
				}
				
				var oF1 = new sap.ui.model.Filter("Event", sap.ui.model.FilterOperator.Contains, event);
		        return new sap.ui.model.Filter({
		            filters: [
		                oF1
		            ],
		            and: false
		        });
			},
			
//*----------------------------------------------------------------------------------------------------------*
//  	*** Proxy...
//*----------------------------------------------------------------------------------------------------------*
			onProxyPress: function(){
				this.byId("idProxyDialog").setVisible(true);
				this.byId("idProxyDialog").open();
				this.getProxy();
			},
			
			handleProxyAddPress: function(){
				this.proxyCreateUpdateFlag = "C";
				var oJSONModel = new sap.ui.model.json.JSONModel({
					Guid:"",
					Evcod:"",
					Pxnam:"",
					Pxrgn:"",
					Strdt2:"",
					Strtm2:"",
					Fnsdt2:"",
					Fnstm2:"",
				});
				this.getView().setModel(oJSONModel,"ProxyCreateModel");
				this.byId("idProxyCreateDialog").setVisible(true);
				this.byId("idProxyCreateDialog").open();
			},
			
			handleProxyEditPress: function(){
				this.proxyCreateUpdateFlag = "U";
				
		    	if(!this.proxyFragmentId){
		            this.proxyFragmentId = this.getView().createId("idProxyFragment");
		    	}
		        var oIdField = sap.ui.core.Fragment.byId(this.proxyFragmentId,"idProxyTable");
		        var selectedItem = oIdField.getSelectedContexts();
		        for(var i in selectedItem){
					var oJSONModel = new sap.ui.model.json.JSONModel({
						Guid:selectedItem[i].getProperty("Guid"),
						Evcod:selectedItem[i].getProperty("Evcod"),
						Pxnam:selectedItem[i].getProperty("Pxnam"),
						Pxrgn:selectedItem[i].getProperty("Pxrgn"),
						Strdt2:selectedItem[i].getProperty("Strdt2"),
						Strdt3:formatter.stringToDate(selectedItem[i].getProperty("Strdt")),
						Strtm2:selectedItem[i].getProperty("Strtm2"),
						Fnsdt2:selectedItem[i].getProperty("Fnsdt2"),
						Fnsdt3:formatter.stringToDate(selectedItem[i].getProperty("Fnsdt")),
						Fnstm2:selectedItem[i].getProperty("Fnstm2"),
					});
					this.getView().setModel(oJSONModel,"ProxyCreateModel");
					this.byId("idProxyCreateDialog").setVisible(true);
					this.byId("idProxyCreateDialog").open();
					return;
		        }
			},
			
			handleProxyCancelPress: function(){
				this.byId("idProxyDialog").close();
				this.byId("idProxyDialog").setVisible(false);
			},
			
			getProxy: function(){
				var t = this;
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");
				
				var sPath = "/ProxySet";
				
				util.showBusy();
				oModel.read(sPath, {
					success: function(oData, response) {
						util.hideBusy();
						var items = oData.results;
						for(var i in items){
							t.formateDate(items[i]);
							// items[i].Strdt2 = formatter.dateFormat(items[i].Strdt);
							// items[i].Strtm2 = formatter.timeFormat(items[i].Strtm);
							// items[i].Fnsdt2 = formatter.dateFormat(items[i].Fnsdt);
							// items[i].Fnstm2 = formatter.timeFormat(items[i].Fnstm);
						}
						var oJSONModel = new sap.ui.model.json.JSONModel({
							ProxyCollection:items
						});
						t.getView().setModel(oJSONModel, "ProxyModel");
					},
					error: function(oError) {
						util.hideBusy();
					}
				});	
			},
			
			formateDate: function(item){
				item.Strdt2 = formatter.dateFormat(item.Strdt);
				item.Strtm2 = formatter.timeFormat(item.Strtm);
				item.Fnsdt2 = formatter.dateFormat(item.Fnsdt);
				item.Fnstm2 = formatter.timeFormat(item.Fnstm);
			},
//*----------------------------------------------------------------------------------------------------------*
//  	*** Proxy Create...
//*----------------------------------------------------------------------------------------------------------*
			handleProxyCreateCancelPress: function(){
				this.byId("idProxyCreateDialog").setVisible(false);
				this.byId("idProxyCreateDialog").close();
			},
			
			handleProxyCreateOkPress: function(){
				var json = this.getView().getModel("ProxyCreateModel");
				this.postProxyOdata();
			},
			
			postProxyOdata: function(){
				var t = this;
				var json = this.getView().getModel("ProxyCreateModel");
				var guid = json.getProperty("/Guid");
				var evcod = json.getProperty("/Evcod");
				var pxnam = json.getProperty("/Pxnam");
				var pxrgn = json.getProperty("/Pxrgn");
				var strdt2 = json.getProperty("/Strdt2");
				var strdt3 = formatter.dateToOdataFormat2(json.getProperty("/Strdt3"));
				var strtm2 = json.getProperty("/Strtm2");
				var fnsdt2 = json.getProperty("/Fnsdt2");
				var fnsdt3 = formatter.dateToOdataFormat2(json.getProperty("/Fnsdt3"));
				var fnstm2 = json.getProperty("/Fnstm2");
				
				if(evcod===""){
					sap.m.MessageToast.show("Event Kod giriniz");
					return;
				}else if(pxnam==="" && pxrgn===""){
					sap.m.MessageToast.show("Kullanıcı ya da Sicil No giriniz");
					return;
				}else if(strdt2===""){
					sap.m.MessageToast.show("Başlangıç Tarihi giriniz");
					return;
				}else if(fnsdt2===""){
					sap.m.MessageToast.show("Bitiş Tarihi giriniz");
					return;
				}
				var strdt = formatter.dateToOdataFormat(strdt3);
				var strtm = formatter.timeToOdataFormat(strtm2);
				var fnsdt = formatter.dateToOdataFormat(fnsdt3);
				var fnstm = formatter.timeToOdataFormat(fnstm2);
				
				var oPostData = {
					Guid:guid,
					Evcod:evcod,
					Type:this.proxyCreateUpdateFlag,
					Evcde:"",
					Pxnam:pxnam,
					Pxrgn:pxrgn,
					Strdt:strdt,
					Strtm:strtm,
					Fnsdt:fnsdt,
					Fnstm:fnstm,
					Return:  "",
					Message: ""
				};
			
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");
				
				var sPath = "/ProxyCreateSet";
				
				util.showBusy();
				oModel.create(sPath, oPostData, {
					success:function(oData, response) {
						util.hideBusy();
						if(oData.Return=="SUCCESS"){
							sap.m.MessageToast.show(oData.Message);
							if(t.proxyCreateUpdateFlag==="C"){
								t.addProxyList(oData);
							}else{
								t.updateProxyList(oData);
							}
							t.byId("idProxyCreateDialog").setVisible(false);
							t.byId("idProxyCreateDialog").close();
							// history.go(-1);
						}else{
							sap.m.MessageToast.show(oData.Message, {
								duration: 5000
							});	
						}
					},
					error:function(err) {
						util.hideBusy();
						// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
					}
				});
			},
			
			addProxyList: function(item){
				var oJSONModel = this.getView().getModel("ProxyModel");
				var items = oJSONModel.getProperty("/ProxyCollection");
				this.formateDate(item);
				items.push(item);
				oJSONModel.setProperty("/ProxyCollection",items);
				this.getView().setModel(oJSONModel,"ProxyModel");
			},
			
			updateProxyList:function(item){
				var oJSONModel = this.getView().getModel("ProxyModel");
				var items = oJSONModel.getProperty("/ProxyCollection");
				this.formateDate(item);
				for(var i in items){
					if(items[i].Guid===item.Guid){
						items[i] = item;
						oJSONModel.setProperty("/ProxyCollection",items);
						this.getView().setModel(oJSONModel,"ProxyModel");
						return;
					}
				}
			},
			
			evcodF4: function(){
				if (!this._oEventCode) {
					this._oEventCode = sap.ui.xmlfragment("appr.view.fragments.EventCode", this);
				}
				this.getEventCode("User");
	
				this._oEventCode.setModel(this.getView().getModel());
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oEventCode);
				this._oEventCode.open();
			},
			
			onEventCodeSelect: function(oEvent){
				var oCell = oEvent.getParameter("selectedItem").getCells()[0];
				var usr = oCell.getTitle();
				this.getView().getModel("ProxyCreateModel").setProperty("/Evcod",usr);
			},
			
			pxnamF4: function(){
				
			},
			
			getEventCode: function(){
				
				var t = this;
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");
				
				var sPath = "/EventCodeSet";
				
				util.showBusy();
				oModel.read(sPath, {
					success: function(oData, response) {
						util.hideBusy();
						var items = oData.results;
						var oJSONModel = new sap.ui.model.json.JSONModel({
							EventCodeCollection:items
						});
						t._oEventCode.setModel(oJSONModel, "EventCodeModel");
					},
					error: function(oError) {
						util.hideBusy();
					}
				});	
			},
			
//*----------------------------------------------------------------------------------------------------------*
//  	*** Proxy Edit...
//*----------------------------------------------------------------------------------------------------------*
			
//*----------------------------------------------------------------------------------------------------------*
//  	*** Proxy Delete...
//*----------------------------------------------------------------------------------------------------------*
			handleProxyDeletePress: function(){
				var t = this;
				var oJSONModel = this.getView().getModel("ProxyModel");
				
		    	if(!this.proxyFragmentId){
		            this.proxyFragmentId = this.getView().createId("idProxyFragment");
		    	}
		        var oIdField = sap.ui.core.Fragment.byId(this.proxyFragmentId,"idProxyTable");
		        var selectedItem = oIdField.getSelectedContexts();
		        var proxyItems = [];
		        for(var i in selectedItem){
		        	var line = {
		        		Guid:selectedItem[i].getProperty("Guid"),
						Evcod:"",
						Evcde:"",
						Pxnam:"",
						Pxrgn:"",
						Strdt:"",
						Strtm:"",
						Fnsdt:"",
						Fnstm:""
		        	};
		        	proxyItems.push(line);
		        }
		        
		        var oPostData = {
					Return:  "",
					Message: "",
					ProxySet:proxyItems
				};
			
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/SAP/ZAPV_01_SRV");
				
				var sPath = "/ProxyDeleteSet";
				
				util.showBusy();
				oModel.create(sPath, oPostData, {
					success:function(oData, response) {
						util.hideBusy();
						if(oData.Return=="SUCCESS"){
							sap.m.MessageToast.show(oData.Message);
							t.deleteProxyList(oData.ProxySet.results);
						}else{
							sap.m.MessageToast.show(oData.Message, {
								duration: 5000
							});	
						}
					},
					error:function(err) {
						util.hideBusy();
						// sap.m.MessageBox.show(message, sap.m.MessageBox.Icon.ERROR);
					}
				});
		        
			},
			
			deleteProxyList: function(proxy){
		    	if(!this.proxyFragmentId){
		            this.proxyFragmentId = this.getView().createId("idProxyFragment");
		    	}
		        var oIdField = sap.ui.core.Fragment.byId(this.proxyFragmentId,"idProxyTable");
		        oIdField.removeSelections();
		     //   var selectedItem = oIdField.getSelectedContexts();
		        var oJSONModel = this.getView().getModel("ProxyModel");
		        var items = oJSONModel.getProperty("/ProxyCollection");
		        var j =0;
		        for(var i in proxy){
		        	for(j =0;j<items.length;j++){
		        		if(proxy[i].Guid===items[j].Guid){
		        			items.splice(j,1);
		        		}
		        	}
		        }
		        oJSONModel.setProperty("/ProxyCollection",items);
		        this.getView().getModel(oJSONModel,"ProxyModel");
			},
			
			liveChangeProxyUname: function(oEvent){
				var oSource = oEvent.getSource();
				var value = oEvent.getParameters().value;
				oSource.setValue(value.toUpperCase());
			}
			
		});

	}
);