jQuery.sap.declare("appr.model.formatter");
sap.ui.define([
	], function () {
		"use strict";

		return {
			/**
			 * Rounds the currency value to 2 digits
			 *
			 * @public
			 * @param {string} sValue value to be formatted
			 * @returns {string} formatted currency value with 2 digits
			 */
			currencyValue : function (sValue) {
				if (!sValue) {
					return "";
				}

				return parseFloat(sValue).toFixed(2);
			},
			
			dateFormat: function(value){
				if(value===""){
					return value;
				}
        		return value.substr(6,2)+"."+value.substr(4,2)+"."+value.substr(0,4);
			},
			
			dateToOdataFormat: function(value){
				if(value===""){
					return value;
				}
        		return value.substr(6,4)+value.substr(3,2)+value.substr(0,2);
			},
			
			stringToDate: function(value){
				if(value===""){
					return new Date();
				}
        		return new Date(value.substr(4,2)+"."+value.substr(6,2)+"."+value.substr(0,4));
			},
			
			timeFormat: function(value){
				if(value===""){
					return value;
				}
        		return value.substr(0,2)+":"+value.substr(2,2);
			},
			
			timeToOdataFormat: function(value){
				if(value===""){
					return "000000";
				}
        		return value.substr(0,2)+value.substr(3,2)+"00";
			},
			
		    dateToOdataFormat2: function(date) {
		        if (date === undefined) return "";
		        var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "dd.MM.yyyy" });
		        return oDateFormat.format(date);
		    },
		};

	}
);