jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 HeaderSet in the list
// * All 3 HeaderSet have at least one DetailSet

sap.ui.require([
	"sap/ui/test/Opa5",
	"appr/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"appr/test/integration/pages/App",
	"appr/test/integration/pages/Browser",
	"appr/test/integration/pages/Master",
	"appr/test/integration/pages/Detail",
	"appr/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "appr.view."
	});

	sap.ui.require([
		"appr/test/integration/MasterJourney",
		"appr/test/integration/NavigationJourney",
		"appr/test/integration/NotFoundJourney",
		"appr/test/integration/BusyJourney",
		"appr/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});