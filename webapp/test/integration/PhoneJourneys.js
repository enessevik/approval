jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"appr/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"appr/test/integration/pages/App",
	"appr/test/integration/pages/Browser",
	"appr/test/integration/pages/Master",
	"appr/test/integration/pages/Detail",
	"appr/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "appr.view."
	});

	sap.ui.require([
		"appr/test/integration/NavigationJourneyPhone",
		"appr/test/integration/NotFoundJourneyPhone",
		"appr/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});